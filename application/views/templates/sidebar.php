<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-solid fa-users-between-lines"></i>
        </div>
        <div class="sidebar-brand-text mx-3">SMK BPI </div>
    </a>

    <!--QUERY MENU -->
    <?php
    $role_id = $this->session->userdata('role_id');
    $queryMenu = "SELECT `siswa_menu`.`id`,`menu`
                     FROM `siswa_menu` JOIN `siswa_access_menu`
                     ON `siswa_menu`.`id` = `siswa_access_menu`.`menu_id`
                     WHERE `siswa_access_menu`.`role_id` = $role_id
                     ORDER BY `siswa_access_menu`.`menu_id` ASC
                     ";

    $menu = $this->db->query($queryMenu)->result_array();
    ?>



    <!-- LOOPING MENU -->
    <?php foreach ($menu as $m) : ?>
        <div class="sidebar-heading">
            <?= $m['menu']; ?>
        </div>

        <!-- SUB-MENU SESUAI MENU -->
        <?php
        $menuId = $m['id'];
        $querySubMenu = "SELECT *
            FROM `siswa_sub_menu` JOIN `siswa_menu`
            ON  `siswa_sub_menu`.`menu_id` = `siswa_menu`.`id`
            WHERE `siswa_sub_menu`.`menu_id` = $menuId
            AND `siswa_sub_menu`.`is_active` = 1
            ";
        $subMenu = $this->db->query($querySubMenu)->result_array();
        ?>

        <?php foreach ($subMenu as $sm) : ?>
            <?php if ($title == $sm['title']) : ?>
                <li class="nav-item active">
                <?php else : ?>
                <li class="nav-item">
                <?php endif; ?>
                <a class="nav-link" href="<?= base_url($sm['url']); ?>">
                    <i class="<?= $sm['icon']; ?>"></i>
                    <span><?= $sm['title']; ?></span></a>
            </li>

        <?php endforeach ?>

        <hr class="sidebar-divider">

    <?php endforeach; ?>



    <li class="nav-item">
        <a class="nav-link" href="<?= base_url('auth/logout'); ?>">
            <i class="fas fa-fw fa-sign-out-alt"></i>
            <span>Logout</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>