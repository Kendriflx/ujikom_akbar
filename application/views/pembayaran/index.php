<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

    <div class="row">
        <div class="col-lg">
            <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>') ?>

            <?= $this->session->flashdata('message'); ?>

            <a href="" class="btn btn-primary mb-3" data-toggle="modal" data-target="#exampleModal">Tambah Pembayaran</a>

            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">nisn</th>
                        <th scope="col">tanggal dibayar</th>
                        <th scope="col">bulan dibayar</th>
                        <th scope="col">tahun dibayar</th>
                        <th scope="col">jumlah bayar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($pembayaran as $p) : ?>
                        <tr>
                            <th scope="row"><?= $i; ?></th>
                            <td><?= $p['nisn']; ?></td>
                            <td><?= $p['tgl_bayar']; ?></td>
                            <td><?= $p['bulan_dibayar']; ?></td>
                            <td><?= $p['tahun_dibayar']; ?></td>
                            <td><?= $p['jumlah_bayar']; ?></td>

                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>



        </div>
    </div>

</div>
</div>
<!-- End of main content-->

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= base_url('pembayaran'); ?>" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" id="nisn" name="nisn" placeholder=" nisn">
                    </div>
                    <div class="form-group">
                        <input type="date" class="form-control" id="tgl_bayar" name="tgl_bayar" placeholder="tanggal dibayar">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="bulan_dibayar" name="bulan_dibayar" placeholder="bulan dibayar">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="tahun_dibayar" name="tahun_dibayar" placeholder="tahun dibayar">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="jumlah_bayar" name="jumlah_bayar" placeholder="jumlah dibayar">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Add</button>
                </div>
            </form>
        </div>
    </div>
</div>