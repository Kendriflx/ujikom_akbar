<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelas extends CI_Controller
{
    public function index()
    {
        $data['title'] = 'Kelas';
        $data['siswa'] = $this->db->get_where('siswa', ['email' =>
        $this->session->userdata('email')])->row_array();

        $data['kelas'] = $this->db->get('kelas')->result_array();

        $this->form_validation->set_rules('nama_kelas', 'nama_kelas', 'required');

        if ($this->form_validation->run() == false) {

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar', $data);
            $this->load->view('kelas/index', $data);
            $this->load->view('templates/footer');
        } else {

            $this->db->insert('kelas', ['nama_kelas' => $this->input->post('nama_kelas'), 'kompetensi_keahlian' => $this->input->post('kompetensi_keahlian')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> New menu added. </div>');
            redirect('kelas/index');
        }
    }
}
