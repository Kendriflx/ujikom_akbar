<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pembayaran extends CI_Controller
{
    public function index()
    {
        $data['title'] = 'Pembayaran';
        $data['pembayaran'] = $this->db->get_where('siswa', ['email' =>
        $this->session->userdata('email')])->row_array();

        $data['pembayaran'] = $this->db->get('pembayaran')->result_array();

        $this->form_validation->set_rules('nisn', 'nisn', 'required');

        if ($this->form_validation->run() == false) {

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar1', $data);
            $this->load->view('pembayaran/index', $data);
            $this->load->view('templates/footer');
        } else {

            $this->db->insert('pembayaran', ['nisn' => $this->input->post('nisn'), 'tgl_bayar' => $this->input->post('tgl_bayar'), 'bulan_dibayar' => $this->input->post('bulan_dibayar'), 'tahun_dibayar' => $this->input->post('tahun_dibayar'), 'jumlah_bayar' => $this->input->post('jumlah_bayar')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> New menu added. </div>');
            redirect('pembayaran/index');
        }
    }
}
