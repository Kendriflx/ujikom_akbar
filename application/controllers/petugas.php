<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Petugas extends CI_Controller
{
    public function index()
    {
        $data['title'] = 'Petugas';
        $data['petugas'] = $this->db->get_where('petugas', ['id_petugas' =>
        $this->session->userdata('email')])->row_array();

        $data['petugas'] = $this->db->get('petugas')->result_array();

        $this->form_validation->set_rules('nama_petugas', 'nama_petugas', 'required');

        if ($this->form_validation->run() == false) {

            $this->load->view('templates/header', $data);
            $this->load->view('templates/sidebar', $data);
            $this->load->view('templates/topbar1', $data);
            $this->load->view('petugas/index', $data);
            $this->load->view('templates/footer');
        } else {

            $this->db->insert('petugas', ['nama_petugas' => $this->input->post('nama_petugas'), 'level' => $this->input->post('level')]);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert"> New menu added. </div>');
            redirect('petugas/index');
        }
    }
    public function create()
    {
        $data['title'] = 'Petugas';
        $data['petugas'] = $this->db->get_where('petugas', ['id_petugas' =>
        $this->session->userdata('email')])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('petugas/index', $data);
        $this->load->view('templates/footer');
    }
}
