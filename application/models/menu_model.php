<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu_model extends CI_Model
{

    public function getSubMenu()

    {
        $query = "SELECT `siswa_sub_menu`.*, `siswa_menu` .`menu`
                    FROM `siswa_sub_menu` JOIN `siswa_menu`
                    ON `siswa_sub_menu`.`menu_id` = `siswa_menu`.`id`
                    ";

        return $this->db->query($query)->result_array();
    }
}
